{
  "nbformat": 4,
  "nbformat_minor": 0,
  "metadata": {
    "colab": {
      "name": "Numerical_1_en.ipynb",
      "provenance": []
    },
    "kernelspec": {
      "name": "python3",
      "display_name": "Python 3"
    },
    "language_info": {
      "name": "python"
    }
  },
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "MkC7Km7YonqC"
      },
      "source": [
        "# Lesson 1. Accuracy of numerical calculations\n",
        "\n",
        "## What is \"numerical calculations\"?\n",
        "\n",
        "\n",
        "Let's start with the classic example - finding the solution (root) of the equation $ f (x) = $ 0:\n",
        "\n",
        "** Exercise 1. ** Find the solution to the equation $3\\cdot x^3+2\\cdot x^2-1=0$. .\n",
        "\n",
        "Let's execute the following code:"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "n5QRIEgEnInS",
        "outputId": "aa45df1f-05db-427b-a87b-3b64472971fd"
      },
      "source": [
        "def f(x):\n",
        "  return 3*x**3+2*x**2-1\n",
        "\n",
        "a, b = 0, 1\n",
        "eps = 1e-6\n",
        "while b - a > eps:\n",
        "  m = (a + b)/2\n",
        "  if f(m) == 0:\n",
        "    break\n",
        "  else:\n",
        "    if f(a) * f(m) > 0:\n",
        "      a=m\n",
        "    else:\n",
        "      b=m\n",
        "print('x = {:.6f}'.format(m))"
      ],
      "execution_count": null,
      "outputs": [
        {
          "output_type": "stream",
          "name": "stdout",
          "text": [
            "x = 0.528182\n"
          ]
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "6NJYq-aM1aPW"
      },
      "source": [
        "Let's analyze how the program works and what exactly it does. First of all - we need to define the range in which we will look for a solution to the equation. If we substitute $ x = 0 $, the left hand side of the equation gives $ f (0) = - $ 1. Similarly, for $ x = $ 1 we get $ f (1) = $ 4. Since we got the values ​​with opposite signs, the root (or more precisely the * odd number * of roots) must appear somewhere between the above values ​​of $ x $. To find it, we will use the bisection algorithm (method), i.e. division in half: we find the middle of the interval and check if it is a root. If not, we check what sign the value of the function has at this point. If it is the same as the right border, then we choose the left half of the range. If it is the same as the left border - we choose the right half.\n",
        "\n",
        "Of course, such an algorithm will work indefinitely (unless we are lucky and hit exactly the root). We should set a certain ** accuracy **, at which we will stop further searching. In the above example, we break the calculations when the width reaches $\\varepsilon = 10^{-6}$ interval and display the result with such precision.\n",
        "\n",
        "In the example above, we didn't solve the equation (we didn't find an analytical solution), but, by calculating specific numerical values, we found an (approximate) solution.\n",
        "\n",
        "Now we can answer the question from the title:\n",
        "\n",
        "**Numerical Calculations** is a method of solving math problems with operations on numbers. The obtained results are approximate, but we can specify the accuracy of the result.\n",
        "We can remark that analytical calculations are usually used by mathematicians or computer scientists to analyse algorithms. In contrast, numerical calculations are the primary tool of engineers.\n",
        "\n",
        "## Errors, inaccuracies, rounding\n",
        "\n",
        "Since numerical calculations are by definition inaccurate, one should wonder what the sources of these inaccuracies are.\n",
        "\n",
        "The first and most obvious one is the **measurement uncertainty**. Most of the values ​​we feed to our computer are representations of some physical quantity from the real world - dimensions, mass, temperature, light brightness and color, loudness and pitch ... they all have to be measured and the measurement is inaccurate. This subject, however, goes far beyond the scope of our course. Therefore, apart from signalling it, we will not deal with it further.\n",
        "\n",
        "The other sources of inaccuracy are already closely related to the way your computer works, so we'll cover them in more detail.\n",
        "\n",
        "### Representation (rounding) errors\n",
        "\n",
        "People write numbers most often in a positional representation of $10$ base. This means that the value of the number depends on the value of the individual digits (from $0$ to $9$) and the position in which those digits are located. The $5$ digit in the last position value is $5$, the same digit in the penultimate position has a value of $50$ and in the second decimal position: $0.05$. In general, the value of an n-digit number can be written as $$ c_{n-1}\\cdot 10^{n-1} + c_{n-2}\\cdot 10^{n-2} + \\ldots + c_{2}\\cdot 10^{2} + c_{1}\\cdot 10^{1} + c_{0}\\cdot 10^{0}=\\sum_{i=0}^{n-1} c_i\\cdot 10^i$$ We write the fractional part by extending the sequence to the right, with indices and negative exponents. In the case of very large or very small numbers, such a notation is not very convenient, as several *significant digits* are followed (preceded) by a large number of zeros. In such a situation, exponential notation is more convenient, e.g. the number *two and a half billion* instead of $2,500,000,000$ can be written as $2.5 \\cdot 10^9$ and *one ten millionth* instead of $0.0000001$ - as $10^{-7}$. Such a notation not only takes up less space, but is also easier to read. Often, an even greater simplification (called engineering notation) is used, replacing $\\cdot 10^{\\square}$ with an **E** or **e**. We will write the above two examples as $2.5 \\mathrm{E}9$ and $1\\mathrm{e}-7$, respectively.\n",
        "\n",
        "The computer uses a binary representation for calculations, i.e. the positional representation with the base $2$, where we have only $0$ and $1$ digits at our disposal, and individual positions mean the appropriate powers of two ($1$, $2$, $4$, $8$, $16$, $\\ldots$, and in the fractional part $\\frac{1}{2}$, $\\frac{1}{4}$, $\\frac{1}{8}$, $\\frac{1}{16}$, $\\ldots$). The equivalent of scientific (engineering) notation will be a floating-point notation conforming to the [IEEE 754](https://en.wikipedia.org/wiki/IEEE_754) standard. For example, a *32-bit* (single-precision) number according to this standard is written as:\n",
        "\n",
        "![image.png](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAk4AAABLCAYAAACGCwhvAAAUk0lEQVR4nO2deZQV1Z3HP92AIDRbNyIgjSwKLjMKgjPJTOISEcmIGTcUJ5nDUZEo7uJyHBca8SRmXMYYVNyNW4xbcAngThB3NC2oIII0ioiNTRsapGnFnj9+VXa96vdqefVeQ/f7fs6pU6/q3u+t33v33Vu/uvWrWyCEEEIIIfJC4/Y2oEA4G3gS6AgMAP4C1DjLbGBggDasjuLWYSfgKccmIbwMAN4GtrTQ8Vqi//G2vaHA01i72wA8AezmybsXMAeodZa5wB4BZattCiFETF6IkOd0YCHWcQO8AlwOdHeWy519mYjT+UaxB6yDft2xTQiXD4GDgQ55Kj/q/zNX+Nve28C5WLvrAfwBeMaT/03gDKCbs0wH3ggoX21TCCFisjokvRRYj13pumwCOnu2d3b2ZSJO5xxmj5e9MNt6xNCIts0XeS4/zv8zKenanp/uwMaA9BLgm4B0tU0h2ijHAx8D9cD72BWli7fhl2HD2PVO/sOxTqMYaA9UA4cBS7Ch/KVOnih0BWY5ZWx0Pu+EORAfABd68l7q7OsG1AHHYJ3OVmAe0NuTtxtwN/Al1tE8CPR00qLYnMmuKPpl2O/XCNyb4XtPAe707XsUu5ItBbo43/fhDHqc8o8FVmG/xxygry89kz1BdQ/2250WcGxRGHSi6b/jLh2AdcBYYAXW/gCGAy9ht7LqsBGdAz1l9QL+ivUdlZ60dP9Pb/+TtC37Sdf2/AxzyklHO+BMUkek/KhtCtFGWY91Xh2Ak4B3PGnejutuoAI7oQ/CnKjvPelbgT8BgzGHZwrW8KNwCzANc866Op+vctL2xeIN9gP2dz7v46RtAx4A+mGd6GXAHz3l3gRc59jcHfgtcGsMm4PsiqIPu+KcC4z27SvBbhm4Heki7KSRiW3AQ1gsRg/gLuC+DDb47Qmqe7CT4lMh30EUDv7/z2bsRL87Tbfv3gH+G2sPXbE26XU+7sXaYQl20fNuQPne7aRt2U+6tuelI7AA+GWG9Ebsgm1AQBlqm0K0Ud7HOqHd06R5G/NaUkdzRtK84Zd7tjtiHUcU1pEahNkTuwpzOQV4D1gMTPQdc7Bnuy+ptxNWkxpYvRvwaQybw+wK04c5Tqsxp8/LjcDFmLPWCziP1BOEn0ZSbzcMAD7PYIPfnqC6B/tunwQcWxQW6RybISGaTqReYH1Jaj8SVr5L0rbsJ13bc+kBvAycH6DfCZgBPBKQR21TiDbKIOAOrEP7O3CQJ83bmBtIDQrtQnDDz7QvHVtpfivAGztQDFQ5Sztf+V6bOtB0ywBsmNub3s7ZF9XmMLvC9GHfv56mW38uX9F0CwJstOmrgDIaaQpuxfmc6Tv67Qmqe7eslnqCSuz4pHNsin379saeUluH/Q+30bwfaR+jfJekbdlPurYH5pB8CEwN0Lp0wW7BZUJtU4g2ThHwK5pfxblUkzr6MpzcOU4rSXUW/FyGxU3Mx54y85Y/yLPdj9QrulWkxhTs5uwLss+7L8yupI7TpzS/6q3FRptcehPuOHlH3QaQuQ4z2ZOu7kFXtSKVIMfGpQobJe2HOSbdfPnWAv2zKD9pW/aTru31Aj4Czsmg6e7bLsH6xUyobQrRBvBfHYI92noCdhX4SYY8YEPX7mPyg0l1YJLyGDAZc1JKsKu9eU7aKGf7FGAScBFNAaWNwBVAH2x4/WQsdsFlNjABuzLsjj1qPDtHdkWhHpvnpWOG9A+xK3Qv87AYkR5YPMcMUr+Tn++xuujj2HhygI1+e8Lqfh8sEF+IqJQAb2FzIQ3FYgLX0XRLbw52K7oHMB4L5nYJai9J27KfdG3vGmzuppsyaF4BLsCcwY5Y/FNQf6C2KUQbZQwWt7MVWA4c6UnzXgWVAy9ij8a/jw0d52rEqTPWWX2ODX3Px4LBuzg2neHJO9XZ594qdJ9aqceecCn15O0K3ION2FRjT9F0jWFzJrui6idgJ431afKBBbDe4dvXCwsoXU/T00OlpKe9Y9ex2JV+HRYwuktEe4LqHiyYdXKGY4vCI8qI00nY6MgW7Im6fYArsUBysNHUeVh7XQL8yKP1/z+95Sdty37StT33tqJ/cSe53Bt4DrtdX4O10zLSo7YphGhGHzI7BC1FnHlSdkTcuWT23N6GpGEYdoIKulUpRGtlR257YahtCtFKWIiN/HQGdsWG4IOeKGkJWrvjBM1nL94R0OzEohDYEdteGGqbQrQihmPzmmzGrtT+jDlQ25O24DiBvXtqNjtGB673YYlCYkdqe2GobQohhBBCCCF2XIryUmpFxXCgioqKr9OnMxyb2FG0PSorKrgX1a8QQuSDygqb4kNsJzJNPJeUG7HXsczPkN7DWVT5bYvhwNGofoUQIh+4fazYjuTLcYrC11RkdKxEa6QCaGrUX1dkdpyFEELExLpYOU7bm0yTWwohhBBCCB9ynIQQQgghIiLHSQghhBAiInKchBBCCCEiIsdJCCGEECIicpyEEEIIISIix0kIIYQQIiL5msfpPKAqIL0STY7YFnHrtQrVrxBC5BqdO4UQQgghROsh23fVDcdeqSGEEEKIlqcSSP8+WJFXsnWcFgEjc2mIEEIIISJzKHqt1XYh2xin9gC7DRlC55KSWMKqpUv5tqGB3kP60LGkUyztpvUb2VS7iT5D+8XSSdty2i+WruG7hu8Y0qcPJZ3i1e/6jRup3bSJof3iH1daaaWVthC0H61dS31DQ2ydyB2JgsMvvf1ORhx8SCzNCcP24POVKzn5zinsdci+sbTP3fgMr943f9v0d69rF0sobYtpL9njTKpXruPOKVM45J/i1e+NTz/DffPnb3v3+vjHlVZaaaUtBO3wC6byXlXQs1ci32g6AiGEEEKIiMhxEkIIIYSIiBwnIYQQQoiIyHESQgghhIiIHCchhBBCBNEN+D3wKdAAfOZsd41ZzoXANqAxQ/ptTlqmZXQazS+AvwEbneV14Pg0+QYAdwCfA1ud7zIPOCKuDXKchBBCCJGJYuCvwKnAZMxZOgU4GXiaaPNBtgduB44meMDGnVj7x065/uUFX/4LgCeBt4DdgRGYc/OoY6PLcOB94D+ASUAZcCywGzAXGB/Hhpw7To/fPJNjBpZzzMByZt82K5b2xZlzuaB8MheUT+blWc/GPvabDy/klOJ0jqa0XpL8zkm0M+fMpfy0yZSfNplZz8av34cXLqT4uPjfN8lxpZVWWmlbizZPjAd+go0wzcNGa54HbgIOxhyQMOYBPUk/YuTFdVrqIpS5J3AN8CJwEVALrAROw0bEDvfkvRpz+M7EHKVN2ETeUzBn6Mo4NuT0Jb8LnpzNsw89yC0vL6Bdhw5M+68J7NK/P/9+5LhQ7buz3+L1Bxdw6YKrad+hHbdOuIHS/mXsP25UpGN/+OISlr70Po2NmUYApYVkv3MS7ew33+LBBQtYcPXVdGjfjgnX30D/sjLGjYpWvy8uXsJLS+J/3yTHlVZaaaVtLdo84o7GzPHtfw64zEl/PKSMJcBU4PuQfHEcp1OBDsBdvv0fYLflvDwPLMVs9vKusx4ax4acjjg9fvNMTp1WQd9Bg+jdvz8TL7ucx2+eGUn74sw5HF1xIrsM6k3P/mUcdfnxvDBzbuRjr3htGRNv+3VWdheSNsnvnEQ7c+4cKiacyKBde9O/rIzLxx/PzDnR6/e1j5Zx2+nxv2+S40orrbTSthZtAEHxOu4SxAhnvcK3392O8vq18wl3miCe4+TOvv12hLy/x0alvvHt7++sV8exIaeO04oli9nrgKbfcNiIA/j4vcpI2s8Wr2bgyME/bO9+wGA+rYw+O+ovrhhPUVF2r94rJG2S3zmJdnHVakYObtIeMHgwlTFmv71ifHbfN8lxpZVWWmlbizaAdHE6/iWIXZ11rW9/jS89F7hOy0RshKgB2IDFMR3gy7uns+4GzHbs2wK8it1ajMIMZ31NHBty6jjV1dbStWfPH7a7lZayccOGSNrNtZvp3LPpvXclpSVs3hDF6RRxSPI7J9HWbt5MT897DUtLSthQl//6TXJcaaWVVtrWos0jOzvrb3373e3OOTxWI/A10A8YA/TCgrl/DLwG/JsnbzdnfT8wCyjH4pr6YE/ZBTlP7YBbsduM9wP3xLEhpzFO6ch2RCWpVkRne9XR9qrf1vh9pZVWWmm3E98AJVg8kfftwh086bmiT5p9T2Dx2H/GRoYOcvZ/5+y/CQs+B1gInI09Bfi/pDpaLj2cssZgztM5pN6uDLUhpyNO/hGmf9TU0K2sLJLWRi82/bC9qaaOkrK4U0SIMJL8zkm0pSUlbNjUpK2pq6Osa/7rN8lxpZVWWmlbizaPfOGsS337eznrz1vABjeo+0DPvi+d9d99eV911iNozlDgTexpwMnYU3XfxbUhp47T0OEjWPbOoh+2P3jzjZSYpyAGjBjEqkVNsWcr31jO7iOH5NI8QbLfOYl2xKBBLFrRpH1j+XJGDsl//SY5rrTSSitta9EGkDQ4/B1nPdS3f5izjhKcHZVhwM+wW2leOjrr+jR29fLlde+kNfj274XdwisBfopNhpmVDTl1nE449zzuumo6X1RV8cWqVdxz9QzGn3NuJO2Y88bx5PRH+KqqmvWrqnlqxmOMOffIXJonSPY7J9Ged9Q4pj/yCFXV1az6spoZjz7GuePyX79JjiuttNJK21q0ASQNDn/YWR/l2+9uP5jUQA9/xOZlOtS3/2fO+iXPvgec9TG+vD911q949vV2yt2K3b4LcvZCbchpjNOPjhjL+jVrOPVfR1FUXMxZv7uWAw8Lm+/K+OexI6hdU8P0URdTVFzEiddOZJ/R+0U+9slFxzX7fE9j2NQShadN8jsn0Y4dMYI1NTWMuuhiiouKuHbiREbvF71+i449rtnnxifCv2+S40orrbTSthZtHnkKmwfpLGzE5nng58AZ2NNs3lk63dGrbAOzLnHKvxX4FTbb90+A67GA7f/x5P2LY9vJ2OjTfcB+wA1Y3NUVnrz/hzlP/0Lq1ANZ2ZDz4PCjTp3EUadOykp70KTRHDQpmqPlJ6rDIW2y3zmJdtLo0UwanZ02ipOUj+NKK6200rYWbZ5oxF6VMh24GeiLxTVdR9Pj/FHKCNrnOlp/w0aErsQCvkuAaiy+aDqwylfGeOz9d+djczVtAhYA04DFnnwnOcd4l8wciM0mHmpD3p+qE0IIIUSr5htsAsmLQvJlGmmKMwK1CHtxbxQagN84SxBxw5ICbdBLfoUQQgghIiLHSQghhBAiInKchBBCCCEiIsdJCCGEECIiiYLDl1dGe4Gvl4YtWwD4tNIfHB9O9Yp11NfVFy2b/4G0O6j22y0251jlqvj1u2LdOurq64vmvx//uNJKK620haDdVF8fnknklWznWqgE9s+lIUIIIYSIzKHA/O1tRCGS7YjT6wmO2Q7YCdgirbTSSiuttNLG5jtsMkYhhBBCCCEKizHYNOzXYjNutpRWtAyqXyGEyB/qJwuM3tgMo+4bl69tIa1oGVS/QgiRP9RPtgJyPR1BP2Bnz/YeLaQVLYPqVwgh8of6yQKkGHspXiOwGTishbSiZVD9CiFE/lA/WaAUA8OBXVpYK1oG1a8QQuQP9ZNCCCGEEEIIIfLD2cCTQEdsyD6IsPRs8+Za3wl4CvtuQghRcOwFzAFqnWUu6YPYJgDf57DMs4DPnOX02FaLqAwFngZqgA3AE8BunvSo9R+1vLD0QuJ0YCHmNEE8Z+WF3JuTQlLHqxM2ea7arhCZ0XmujfImcAbQzVmmA2/48hwG3E70zjaszKOxTncQ0B87uYzLznwRwtvAuUB3oAfwB+AZT3qU+o9TXlh6oVAKrMccSZc4zsrq3JrTjKSOE5jTvR6rZyFEKjrPFRAl2NwTXq7A3oWXbWfrL/MF4AjP9ljs6QORf7oDGwPS09V/kvLC0tsqU4A7ffsagWOBVUAdNtLX15cOsIymuV/uzVC+m7c9UI1d3CzBXvuwFDjck7cXdrtwo3PsY0hty12BWU45G53POwGdgQ+ACz15L3X2dXa27wZOy2CjEIWMznMFQjvgTDKPEGTjOKUrsxrrzF16A+uyKFvEZxh2Yk1HWP3HLS9KeltlLjDat28b8BB267IHcBdwnye9McPndHjTtwJ/AgZjDs0U4GNP+h3ANKAn5qjdT+pt91uc9DLMiZoGXOWk7Yvdct0Pexn4BmAfj3YsFu8khEhF57kCoRG7RTAgID0XZX6LnaRd2gMNWZQt4tERWAD8MkN6WP3HLS8svS2zGpsAz0sjqbfuBgCf+9LTfU6HP2+5Z7sj5qS5rCH1cejBPv06UuPQemKjXi6nAO8Bi4GJPjvKgU9CbBWiENF5rkDYCZgBPJIhPRvHKV2Z6f5QW7MoW0SnB/AycH5AnrD6j1NelOO1Zeqx39NLI02B4jif633p6T6nIyyvf0Sqve+4/vRG3+K9XVsMVDmLt926ZWX7Vngh2jI6zxUQXbD4i3RkG+PkL/NLUq+AdwXWZlm2CKcc+BCYGiFvUP1HLS/O8doqn5J+xGmwZ3uAk8+bnu5zOuI4TmuxYHWXob70ldgoUyYuA14C5gOX+9I04iREenSea8N0922XYPdm0xHVcQorcx7wc8/2f2KPsIvc0wv4CDgnQ3qc+o9SXlh6oTCP5q9Y2IYFU/fBfudp2NOqLt72tQWbFsI7QkWGvGGO033AJdgoYF/gAVJjnH7npPd07JpKUxDrKCyuaaBjzz+AAz3aI1DbFSIdOs+1YRYDF2CPoncEfk1qwKqXqI5TWJljsUfeB2KPai6ieSCtyA13Ar8NSI9T/1HKC0svFKZgQdku7bGRvGOxW151WFC194rU274mYLFH6zOUH8dx6os9wVeHxV4dh92Kc18M3hm4CYu3qsNGlvbDRh+XY9NVuEx19nVxtu8CJmewUYhCRue5NszewHNYR1qDPfVT5svjj38Ic6CilDkJ+Aob3fAHnIrcsY309edOchmlruKUF5ZeKLjzOO25vQ3JI8Ow9ht0m0+IQkbnOSGEiIF/5vC2hGYOF0K0ev4fPIt1WQ6/gwkAAAAASUVORK5CYII=)  \n",
        "\n",
        "where the highest bit (MSB) encodes the sign of the number ($0$ for positive numbers and $1$ for negative numbers), the next $8$ bits encode the exponent (in BIAS-127 notation, which allows numbers from $-126$ to $127$). The remaining $23$ bits are used to store the fractional part of the significant digits, i.e. the *mantissa*. We can easily calculate that the mantissa of a 32-bit number can hold about $7$ significant digits of a decimal number.\n",
        "\n",
        "Conversion from decimal to binary, on the other hand, is usually infinite, so the conversion should be truncated or rounded when the bit limit is reached. So the first source of inaccuracy is ** the rounding of the decimal to binary floating-point conversion **.\n",
        "\n",
        "### Accuracy of arithmetic operations\n",
        "\n",
        "Further problems arise if we try to perform arithmetic operations on the numbers written in this way. The multiplication and division of floating-point numbers is the multiplication (division) of the mantissa and the addition (subtraction) of the exponents. In case of very large or small exponents, their sum (difference) might exceed the available range. In this case, we are talking about an overflow or a deficiency error. Still, python can deal with them, and the result will be presented as either infinity or zero. Let's check the above with an example:"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "qUKei7u45T5o",
        "outputId": "8ee480e1-39b8-4d51-f636-d80239fcba41"
      },
      "source": [
        "print(1e180 * 1e128)\n",
        "print(1e180 * 2e128)\n",
        "print(1e-180 / 1e143)\n",
        "print(1e-180 / 5e143)"
      ],
      "execution_count": null,
      "outputs": [
        {
          "output_type": "stream",
          "name": "stdout",
          "text": [
            "1e+308\n",
            "inf\n",
            "1e-323\n",
            "0.0\n"
          ]
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "rUo4gdjv56CR"
      },
      "source": [
        "Integer operations in numeric packages like `numpy` or` pandas` are a bigger problem - integer overflow can change the sign of the result and produce a completely wrong result:"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "fwvwv0eK4tPM",
        "outputId": "ee941aed-3aa5-49bd-9307-847ca67584a9"
      },
      "source": [
        "import numpy as np\n",
        "a = np.array([2**63 - 1], dtype=int)\n",
        "print(a)\n",
        "print(a+1)"
      ],
      "execution_count": null,
      "outputs": [
        {
          "output_type": "stream",
          "name": "stdout",
          "text": [
            "[9223372036854775807]\n",
            "[-9223372036854775808]\n"
          ]
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "KEIoDWaQ6j4K"
      },
      "source": [
        "The operations of addition and subtraction are even more interesting because inaccuracies propagate very quickly there. Let's try to add two quite ordinary numbers:"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "Io04IEe2oj1h",
        "outputId": "0d73a32d-23ab-4822-9a64-47d9ca63ead1"
      },
      "source": [
        "print(0.1)\n",
        "print(0.2)\n",
        "print(0.1 + 0.2)"
      ],
      "execution_count": null,
      "outputs": [
        {
          "output_type": "stream",
          "name": "stdout",
          "text": [
            "0.1\n",
            "0.2\n",
            "0.30000000000000004\n"
          ]
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "pdSZFgYV7HbY"
      },
      "source": [
        "Adding (subtracting) floating-point numbers is quite a complicated operation - it requires prior agreement (normalization) of the exponents, which involves multiplying or dividing the mantissa of one of the numbers. In algorithms built into programming languages, the second and subsequent numbers are normalized to the first, so the result of the addition may depend on the order in which the numbers are added. Let's look at it in the example below - let's add a small number to $1$ a few times, and then change the order of the numbers:"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "1BXs8Z6Wv1Vn",
        "outputId": "5451f815-19ec-4ceb-c250-dc2e5b32b0ce"
      },
      "source": [
        "a=1.0 + 1e-16 + 1e-16 + 1e-16 + 1e-16 + 1e-16\n",
        "b=1e-16 + 1e-16 + 1e-16 + 1e-16 + 1e-16 + 1.0\n",
        "print(\"a= {}, b={}, a-b={}\".format(a, b, b-a))"
      ],
      "execution_count": null,
      "outputs": [
        {
          "output_type": "stream",
          "name": "stdout",
          "text": [
            "a= 1.0, b=1.0000000000000004, a-b=4.440892098500626e-16\n"
          ]
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "YQ3XxpFL8xJx"
      },
      "source": [
        "The most important conclusion from the above is that even if the individual arguments are expressed precisely, it is not certain that the result of the action will also be accurate. The second consequence is the phenomenon of a **numerical instability**. It appears when a small numerical error multiplies in further calculations and causes a significant error in the result.\n",
        "Let's try doing addition and subtraction multiple times and see how this affects the result:"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "EcWJsA1OBOC7",
        "outputId": "47a4ccd0-1425-4404-fa84-470d3e3da1b3"
      },
      "source": [
        "def add_and_subtract(iterations):\n",
        "    result = 1\n",
        "    \n",
        "    for i in range(iterations):\n",
        "        result += 1/3\n",
        "\n",
        "    for i in range(iterations):\n",
        "        result -= 1/3\n",
        "    return result\n",
        "\n",
        "# tylko raz:\n",
        "print(\" Jeden raz:\",add_and_subtract(1))\n",
        "\n",
        "# 100 razy:\n",
        "print(\"  100 razy:\", add_and_subtract(100))\n",
        "\n",
        "# 10000 razy:\n",
        "\n",
        "print(\"10000 razy:\",add_and_subtract(10000))\n"
      ],
      "execution_count": null,
      "outputs": [
        {
          "output_type": "stream",
          "name": "stdout",
          "text": [
            " Jeden raz: 1.0\n",
            "  100 razy: 1.0000000000000002\n",
            "10000 razy: 1.0000000000001166\n"
          ]
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "OstlwBugCnif"
      },
      "source": [
        "### Limit value and cut-off errors\n",
        "\n",
        "When analyzing the example, we noticed that the algorithm for calculating the root of the equation could run indefinitely. It should be artificially limited by assuming a specific accuracy limit. Another example of such an arbitrary constraint is calculating the sum (or expansion) of an infinite series - of course, the computation should be limited to a finite number of terms. Inaccuracies introduced in this way are called cut-off errors. The good news is that we are usually able to determine for ourselves how big they should be. It is always the result of a compromise between the required precision of the result and the use of resources (time and memory) to calculate this result.\n",
        "\n",
        "# Summary\n",
        "\n",
        "1. Numbers are represented by representations. Each representation has some advantages and disadvantages.\n",
        "1. Computers use binary representation with a limited number of bits, which causes cumulative errors.\n",
        "1. Errors and rounding are essential and inseparable elements of numerical calculations and should always be considered."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "X-o2KrT3tqW5"
      },
      "source": [
        ""
      ],
      "execution_count": null,
      "outputs": []
    }
  ]
}